This is a jupyter notebook coding environment.

If you wish to run this environment then my instruction are for unix like operating systems with python and pip installed.

```bash
# To run the python environment
# From the root of the directory
pip install virtualenv
source venv/bin/activate

# To run the jupyter environment
jupyter notebook

# Your default browser should then launch and connect to it
# If not then connect via your favourite graphical browser via "localhost:8888"
```
